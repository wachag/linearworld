#ifndef WORLD_H
#define WORLD_H

#include <QColor>
#include <QList>
#include <QMap>
#include <QObject>
#include <QVector>
#include <QtGlobal>
#include <yaml-cpp/yaml.h>
struct Resource {
  std::string id;
  uint32_t quantum;
};

struct TerrainResource {
  Resource resource;
  uint32_t probability;
};

struct Terrain {
  std::string id;
  std::string icon;
  uint32_t elevationMean;
  uint32_t elevationVar;
  std::string color;
  std::vector<TerrainResource> potentialResources;
};

struct Place{
    enum SizeEnum{SMALL=0,MEDIUM=1,LARGE=2,MAXSIZE=3} size; // "items" in site will be 1<<size;
    Terrain terrain;
    QList<Resource> resources;
};


class World : public QObject {
  static const uint32_t MAX_ELEVATION = 1200;
  static const uint32_t START_SITE_SIZE = 5;
  Q_OBJECT
  Q_PROPERTY(quint32 worldSize READ getWorldSize WRITE setWorldSize NOTIFY
                 worldSizeChanged);
  Q_PROPERTY(quint32 explorationSize READ getExplorationSize);
  Q_PROPERTY(
      QVector<qreal> elevation READ getElevation NOTIFY elevationChanged);

  Q_PROPERTY(
      int octaves READ getOctaves WRITE setOctaves NOTIFY octavesChanged);
  Q_PROPERTY(double frequency READ getFrequency WRITE setFrequency NOTIFY
                 frequencyChanged);

  QMap<QString, Resource> resourceKind;
  QMap<QString, Terrain> terrainKind;
  QVector<qreal> elevation;
  QVector<Place> places;
  uint32_t worldSize;
  uint32_t octaves;
  double frequency;

public:
  explicit World(QObject *parent = nullptr);
  void setWorldSize(uint32_t ws) {
    worldSize = ws;
    emit worldSizeChanged();
    initialize();
  }
  quint32 getWorldSize() const { return worldSize; }
  quint32 getExplorationSize() const { return 3; }

  void setOctaves(uint32_t o) {
    octaves = rand() % 20;
    emit octavesChanged();
    initialize();
  }
  uint32_t getOctaves() const { return octaves; }
  void setFrequency(double f) {
    frequency = f;
    emit frequencyChanged();
    initialize();
  }
  double getFrequency() const { return frequency; }

  Q_INVOKABLE QString getTerrainName(quint32 index) const {
    return QString::fromStdString(places[index].terrain.id);
  }
  Q_INVOKABLE QString getTerrainColor(quint32 index) const {
    return QString::fromStdString(places[index].terrain.color);
  }

  Q_INVOKABLE QString getTerrainIcon(quint32 index) const {
    return QString::fromStdString(places[index].terrain.icon);
  }

  Q_INVOKABLE quint8 getPlaceSize(quint32 index) const {
    return (1<<places[index].size);
  }
  QVector<qreal> getElevation() const { return elevation; }
signals:
  void worldSizeChanged();
  void elevationChanged();
  void octavesChanged();
  void frequencyChanged();
  void initialized();
public slots:

  void initialize();
};

#endif // WORLD_H

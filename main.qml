import QtQuick 2.12
import QtQuick.Controls 2.5
import QtCharts 2.3
import hu.wachag.LinearEmpire 1.0
import "WorldForm.js" as WorldScript

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Linear Empire")
    World {
        id: worldModel
        frequency:  debugForm.frequency.realValue
        octaves: debugForm.octaves.value
        onInitialized: {
            worldForm.world.model.clear()
            WorldScript.explore(worldForm.world, worldModel,5)

        }

        onElevationChanged: {
            var elev = elevation

            debugForm.chartView.removeAllSeries()
            debugForm.xAxis.min=0
            debugForm.xAxis.max=elev.length
            var series = debugForm.chartView.createSeries(
                        ChartView.SeriesTypeLine, "elevation",debugForm.xAxis,debugForm.yAxis)
            var i
            series.pointsVisible = true
            series.color = Qt.rgba(Math.random(), Math.random(),
                                   Math.random(), 1)
            for (i = 0; i < elev.length; i++){
                series.append(i, elev[i])
            }
            debugForm.TopRight
        }
    }
    Component.onCompleted: {
        worldModel.initialize()
    }
    SwipeView {
        id: swipeView
        opacity: 1.0
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        background: Image {
                    id: background
                    anchors.fill: parent
                    source: "background.png"
                    fillMode: Image.PreserveAspectCrop
                }

        WorldForm {

            id: worldForm
            button.onClicked: {
                WorldScript.explore(world, worldModel)
            }
        }

        EmpireForm {}
        DebugForm {
            id: debugForm

        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("World")
        }
        TabButton {
            text: qsTr("Empire")
        }
        TabButton {
            text: qsTr("Debug")
        }
    }
}

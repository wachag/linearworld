import QtQuick 2.12
import QtQuick.Controls 2.5
import QtCharts 2.3
import QtQuick.Layouts 1.0

Page {
    width: 600
    height: 400
    property alias octaves: octaves
    property alias frequency: frequency
    property alias chartView: chartView
    property alias xAxis: xAxis
    property alias yAxis: yAxis
    header: Label {
        text: qsTr("Debug")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }
    ColumnLayout {
        anchors.fill: parent
        id: layout
        RowLayout {
            id: rl
            SpinBox {
                id: frequency
                property real realValue: value / 10
                Layout.fillWidth: true
                from: 0
                value: 40
                to: 640
            }
            SpinBox {
                id: octaves
                Layout.fillWidth: true
                value: 0
                from: 0
                to: 640
            }
        }

        ChartView {
            id: chartView
            title: "Line"
            antialiasing: true
            Layout.fillHeight: true
            Layout.fillWidth: true
            axes: [
                ValueAxis {
                    id: xAxis
                    min: 0.0
                    max: 1000.0
                },
                ValueAxis {
                    id: yAxis
                    min: 0.0
                    max: 1500.0
                }
            ]
        }
    }
}

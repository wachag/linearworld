import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.0

Page {
    width: 600
    height: 400

    property alias button: button
    property alias world: world

    header: Label {
        text: qsTr("World")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    ColumnLayout {
        anchors.fill: parent
        id: layout
        Button {
            opacity: 1.0
            height: 30
            id: button
            text: qsTr("Explore")
        }
        spacing: 10
        ScrollView {
            background: Image {
                id: background
                anchors.fill: parent
                source: "background.png"
                fillMode: Image.PreserveAspectCrop
            }

            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true

            ListView {
                id: world
                //  Layout.fillHeight: true
                //  Layout.fillWidth: true
                model: ListModel {
                    id: model
                }
                delegate: Item {
                    width: parent.width
                    height: childrenRect.height + 10
                    Column {
                        Row {
                            id: row1
                            Image {
                                opacity: 1.0
                                width: 64
                                height: 64
                                id: iconImage
                                source: icon
                            }

                            Text {
                                opacity: 1.0
                                text: name
                                font.bold: true
                                anchors.verticalCenter: parent.verticalCenter
                                color: "white"
                            }
                        }
                        Grid {
                            x: 4
                            y: 2
                            width: childrenRect.width
                            height: childrenRect.height
                            Repeater {

                                model: placeSize
                                width: childrenRect.width
                                height: childrenRect.height
                                Row {
                                    Image {
                                        opacity: 1.0
                                        id: siteIcon
                                        source: "emptysite.png"
                                        width: 64
                                        height: 64
                                        property int site: index
                                        property int place: placeIndex
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                console.log(index, " ",
                                                            placeIndex)
                                            }
                                        }
                                    }
                                    Text {
                                        //                                width: 40
                                        //   height: 40
                                        // anchors.centerIn: siteIcon
                                        anchors.verticalCenter: parent.verticalCenter

                                        text: "empty"
                                        color: "blue"
                                        property int site: index
                                        property int place: placeIndex
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                console.log(index, " ",
                                                            placeIndex)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //                        spacing: 10
                    }
                }
            }
        }
    }
}

#include "world.h"
#include "perlin.h"
#include <QFile>
#include <iostream>
#include <yaml-cpp/yaml.h>
World::World(QObject *parent) : QObject(parent) {
  // std::cout << "Loading resources\n";
  QFile resourceFile(":/resources.yaml");
  if (!resourceFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
    return;
  }
  YAML::Node resources = YAML::Load(resourceFile.readAll());
  for (auto resource : resources["resources"]) {
    Resource r;
    r.id = resource["id"].as<std::string>();
    r.quantum = resource["quantum"].as<uint32_t>();
    resourceKind.insert(QString::fromStdString(r.id), r);
  }
  //  std::cout << "Loading terrain\n";
  QFile terrainFile(":/terrain.yaml");
  if (!terrainFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
    return;
  }
  YAML::Node terrains = YAML::Load(terrainFile.readAll());
  Terrain startAndEnd;
  startAndEnd.id = "Headquarters";
  startAndEnd.icon = "headquarters.png";
  startAndEnd.color = "#FFFFFF";
  startAndEnd.elevationVar = 0;
  startAndEnd.elevationMean = 0xffffffff;
  terrainKind.insert("Headquarters", startAndEnd);
  for (auto terrain : terrains["terrains"]) {
    Terrain t;
    t.id = terrain["id"].as<std::string>();
    TerrainResource tr;
    // std::cout << t.id << ":\n";
    std::stringstream resourceId;
    std::stringstream probabilities;
    resourceId << terrain["resources"].as<std::string>();
    if (t.id == "Headquarters")
      continue;
    probabilities << terrain["resourceprobability"].as<std::string>();
    t.elevationVar = terrain["elevationvar"].as<uint32_t>();
    t.elevationMean = terrain["elevationmean"].as<uint32_t>();
    t.color = terrain["color"].as<std::string>();
    t.icon =terrain["icon"].as<std::string>();
    while (resourceId && probabilities) {
      TerrainResource tr;
      uint32_t probability;
      std::string id;
      resourceId >> id;
      probabilities >> probability;
      if (!probabilities || !resourceId)
        break;
      tr.resource = resourceKind[QString::fromStdString(id)];
      tr.probability = probability;
      t.potentialResources.push_back(tr);
      //      std::cout << id << " " << probability << "\n";
    }

    terrainKind.insert(QString::fromStdString(t.id), t);
  }
  octaves = 1;
  setWorldSize(20);
  frequency = getWorldSize() * 2;
}

void World::initialize() {
  /*
   generate elevation map
  */
  const siv::PerlinNoise perlin;
  elevation.clear();
  const double fx = worldSize / frequency;
  for (uint32_t i = 0; i < worldSize; i++) {
    if (i < START_SITE_SIZE) { // "plain elevation in the first few items"
      elevation.push_back(fabs(MAX_ELEVATION * 0.5 - MAX_ELEVATION / 4));
    } else if (i > worldSize - 1 - START_SITE_SIZE) {
      elevation.push_back(elevation[worldSize - i - 1]);
    } else {
      elevation.push_back(
          fabs(MAX_ELEVATION * perlin.octaveNoise0_1(i / fx, octaves) -
               MAX_ELEVATION / 4));
    }
  }
  emit elevationChanged();
  /*
    for each elevation get a random value less then the value of the density
   function select the maximum
  */
  places.clear();
  std::random_device rd;
  std::mt19937 gen(rd());
  for (uint32_t i = 0; i < worldSize; i++) {
    double maximum = 0;
    Place place;
    // First item is always the HQ
    if (i == 0) {
      place.terrain = terrainKind["Headquarters"];
      place.size=Place::SMALL;
      places.push_back(place);
      continue;
    }
    // Mirror the start site
    if (i > worldSize - 1 - START_SITE_SIZE) {
      places.push_back(places[worldSize - i - 1]);
      continue;
    }
    for (auto terrain : terrainKind.values()) {
      double gaussian = (1 / (terrain.elevationVar * sqrt(2 * M_PI))) *
                        exp(-0.5 * pow((elevation[i] - terrain.elevationMean) /
                                           terrain.elevationVar,
                                       2.0));
      std::uniform_real_distribution<> dis(0, gaussian);
      double prob = dis(gen);
      if (maximum < prob) {
        maximum = prob;
        place.terrain = terrain;
      }
    }
    // TODO: generate resources
    std::uniform_int_distribution<> sizeDis(0,Place::MAXSIZE-1);
    place.size= (Place::SizeEnum)sizeDis(gen);
    places.push_back(place);
  }
  emit initialized();
}

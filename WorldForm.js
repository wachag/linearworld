function min(a, b) {
    if (a > b)
        return b
    return a
}
function explore(world, worldModel, step) {
    //  console.log(world.model.count)
    var i
    console.log(worldModel.explorationSize, worldModel.worldSize)
    if (step == 0)
        step = worldModel.explorationSize
    for (i = 0; i < step; i++) {
        if (world.model.count < worldModel.worldSize) {
            var elem
            world.model.append({
                                   "icon": worldModel.getTerrainIcon(
                                               world.model.count),
                                   "placeIndex": world.model.count,
                                   "placeSize": worldModel.getPlaceSize(
                                                    world.model.count),
                                   "colorCode": worldModel.getTerrainColor(
                                                    world.model.count),
                                   "name": worldModel.getTerrainName(
                                               world.model.count)
                               })
        }
    }
}
